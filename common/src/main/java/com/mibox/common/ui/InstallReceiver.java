package com.mibox.common.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.blankj.utilcode.util.LogUtils;
import com.mibox.common.event.PackageEvent;

import org.greenrobot.eventbus.EventBus;

public class InstallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtils.d("Package List Changed!!");
        EventBus.getDefault().post(new PackageEvent(intent.getAction(), intent.getDataString()));
    }
}
