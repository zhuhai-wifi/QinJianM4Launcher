package com.mibox.common.ui;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.mibox.common.R;
import com.mibox.common.databinding.ActivitySleepClockBinding;

public class SleepClockActivity extends Activity {

    private ActivitySleepClockBinding mB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mB = ActivitySleepClockBinding.inflate(getLayoutInflater());
        setContentView(mB.getRoot());
        mB.clockTime.setTypeface(ResourcesCompat.getFont(this, R.font.font_lcd));
    }
}
