package com.mibox.common.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mibox.common.consts.MMKVKey;
import com.mibox.common.databinding.ActivityStockSetBinding;
import com.tencent.mmkv.MMKV;

public class StockSetActivity extends AppCompatActivity {
    private ActivityStockSetBinding mB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mB = ActivityStockSetBinding.inflate(getLayoutInflater());
        setContentView(mB.getRoot());
        mB.btnSave.setOnClickListener(v -> {
            saveStockList();
            finish();
        });
        mB.editCode.setText(MMKV.defaultMMKV().getString(MMKVKey.KEY_STOCK_CODES, ""));
    }

    private void saveStockList() {
        String codes = mB.editCode.getText().toString().trim();
        MMKV.defaultMMKV().putString(MMKVKey.KEY_STOCK_CODES, codes);
    }
}
