package com.mibox.common.ui;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.SeekBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ShellUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.mibox.common.R;
import com.mibox.common.adapter.SettingsGridAdapter;
import com.mibox.common.app.SettingsEntry;
import com.mibox.common.databinding.ActivityConfigBinding;

import java.util.LinkedList;

public class ConfigActivity extends AppCompatActivity {

    private ActivityConfigBinding mB;
    private ContentResolver mResolver;
    private SettingsGridAdapter mSettingsAdapter;
    private LinkedList<SettingsEntry> mSettingsItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mB = ActivityConfigBinding.inflate(getLayoutInflater());
        setContentView(mB.getRoot());
        mB.seekBright.setOnSeekBarChangeListener(getBrightChangeListener());
        mResolver = getContentResolver();
        if (Settings.System.canWrite(this)) {
            readScreenBrightness();
        } else {
            ToastUtils.showShort("请先打开系统设置权限");
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            finish();
        }
        setupRecyclerList();
    }

    private void setupRecyclerList() {
        mB.settingsEntryList.setLayoutManager(new GridLayoutManager(this, 3));
        mSettingsAdapter = new SettingsGridAdapter();
        mB.settingsEntryList.setAdapter(mSettingsAdapter);
        mSettingsItems = new LinkedList<>();
        mSettingsItems.add(new SettingsEntry("股市设置", () ->
                ActivityUtils.startActivity(StockSetActivity.class)));
        mSettingsItems.add(new SettingsEntry("重启机器", () ->
                ShellUtils.execCmd("reboot", true)));
        mSettingsAdapter.setOnItemClickListener((adapter, view, position) ->
                mSettingsItems.get(position).listener.onClick());
        mSettingsAdapter.setList(mSettingsItems);
    }

    private void readScreenBrightness() {
        try {
            int brightness = Settings.System.getInt(mResolver, Settings.System.SCREEN_BRIGHTNESS, 0);
            mB.seekBright.setProgress(brightness);
            showBrightText(brightness);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeScreenBrightness(int brightness) {
        try {
            Settings.System.putInt(mResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
            showBrightText(brightness);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SeekBar.OnSeekBarChangeListener getBrightChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (Settings.System.canWrite(ConfigActivity.this)) {
                    writeScreenBrightness(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void showBrightText(int brightness) {
        mB.txtBright.setText(getString(R.string.screen_brightness) + String.format("(%d)", brightness));
    }
}
