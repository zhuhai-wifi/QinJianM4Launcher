package com.mibox.common.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amap.api.location.AMapLocation;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.mibox.common.adapter.Day15Adapter;
import com.mibox.common.adapter.Hour24Adapter;
import com.mibox.common.bean.BriefDataMsg;
import com.mibox.common.bean.CityId;
import com.mibox.common.bean.Day15Brief;
import com.mibox.common.bean.Hour24Brief;
import com.mibox.common.databinding.ActivityWeatherBinding;
import com.mibox.common.utils.HttpApi;
import com.mibox.common.utils.HttpCallback;
import com.mibox.common.utils.location.AMapCallback;
import com.mibox.common.utils.location.AMapHelper;
import com.mibox.common.utils.ticker.AppTicker;
import com.mibox.common.utils.ticker.TickCallback;

import java.util.LinkedList;
import java.util.List;

public class WeatherActivity extends AppCompatActivity
        implements AMapCallback, TickCallback {

    private ActivityWeatherBinding mB;
    private CityId mCityId;
    private BriefDataMsg.BriefData mBriefData;
    private String mLocationString = "";
    private int mDisplaySeconds = 0;
    private List<Hour24Brief.Hour24Data> m24HourList = new LinkedList<>();
    private Hour24Adapter mHour24Adapter;
    private Day15Adapter mDay15Adapter;
    private List<Day15Brief.Day15Data> m15DaysList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mB = ActivityWeatherBinding.inflate(getLayoutInflater());
        setContentView(mB.getRoot());
        initPageListView();
        loadLocationInfo();
    }

    private void initPageListView() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.HORIZONTAL);
        mB.hour24List.setLayoutManager(manager);
        mHour24Adapter = new Hour24Adapter();
        mB.hour24List.setAdapter(mHour24Adapter);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.HORIZONTAL);
        mB.day15List.setLayoutManager(manager);
        mDay15Adapter = new Day15Adapter();
        mB.day15List.setAdapter(mDay15Adapter);
    }

    @Override
    public void onSecondTick(long sec) {
        mDisplaySeconds += 1;
        if (mDisplaySeconds % 600 == 0) {
            loadRealTimeBrief();
            load24HourBrief();
            load15DaysBrief();
        }
    }

    private void load15DaysBrief() {
        if (mCityId != null) {
            HttpApi.get15DaysBrief(mCityId.cityId, new HttpCallback<Day15Brief>() {
                @Override
                public void onSuccess(Day15Brief res) {
                    m15DaysList = res.data;
                    runOnUiThread(() -> show15DaysBrief());
                }

                @Override
                public void onFail(String exp) {
                    ToastUtils.showShort(exp);
                }
            });
        }
    }

    private void show15DaysBrief() {
        mDay15Adapter.setList(m15DaysList);
    }

    private void loadLocationInfo() {
        AMapHelper.INSTANCE.fetch(this, false);
    }

    private void loadRealTimeBrief() {
        if (mCityId != null) {
            HttpApi.getRealTimeBrief(mCityId.cityId, new HttpCallback<BriefDataMsg>() {
                @Override
                public void onSuccess(BriefDataMsg res) {
                    mBriefData = res.data;
                    runOnUiThread(() -> showRealTimeBrief());
                }

                @Override
                public void onFail(String exp) {
                    ToastUtils.showShort(exp);
                }
            });
        }
    }

    private void load24HourBrief() {
        if (mCityId != null) {
            HttpApi.get24HourBrief(mCityId.cityId, new HttpCallback<Hour24Brief>() {
                @Override
                public void onSuccess(Hour24Brief res) {
                    m24HourList = res.data;
                    runOnUiThread(() -> show24HourBrief());
                }

                @Override
                public void onFail(String exp) {
                    ToastUtils.showShort(exp);
                }
            });
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void show24HourBrief() {
        mHour24Adapter.setList(m24HourList);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void showRealTimeBrief() {
        mB.title.setText(mLocationString + String.format("%d℃ %s",
                mBriefData.temperature, mBriefData.condition));
    }

    @Override
    public void onLocationGet(AMapLocation location) {
        if (location.getLatitude() == 0) {
            ThreadUtils.runOnUiThreadDelayed(this::loadLocationInfo, 1000);
        } else {
            mLocationString = location.getCity() + " " + location.getDistrict();
            HttpApi.getCityId(location, new HttpCallback<CityId>() {
                @Override
                public void onSuccess(CityId res) {
                    mCityId = res;
                    loadRealTimeBrief();
                    load24HourBrief();
                    load15DaysBrief();
                }

                @Override
                public void onFail(String exp) {
                    ToastUtils.showShort(exp);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDisplaySeconds = 0;
        AppTicker.INSTANCE.register(this);
    }

    @Override
    protected void onPause() {
        AppTicker.INSTANCE.unregister(this);
        super.onPause();
    }
}
