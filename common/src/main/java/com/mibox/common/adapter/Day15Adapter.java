package com.mibox.common.adapter;

import android.annotation.SuppressLint;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mibox.common.R;
import com.mibox.common.bean.Day15Brief;

public class Day15Adapter extends BaseQuickAdapter<Day15Brief.Day15Data, BaseViewHolder> {

    public Day15Adapter() {
        super(R.layout.layout_day15_item);
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void convert(@NonNull BaseViewHolder vh, Day15Brief.Day15Data data) {
        vh.setText(R.id.txt_week, String.format("%s (%s)", data.week, data.date));
        ImageView iconView = vh.getView(R.id.icon_min);
        Glide.with(iconView.getContext()).load(data.nightIcon).into(iconView);
        iconView = vh.getView(R.id.icon_max);
        Glide.with(iconView.getContext()).load(data.dayIcon).into(iconView);
        vh.setText(R.id.txt_temp, String.format("%d℃ ~ %d℃", data.minTemperature, data.maxTemperature));
        vh.setText(R.id.txt_cond, String.format("%s ~ %s", data.dayCondition, data.nightCondition));
        vh.setText(R.id.txt_wind, String.format("%s %d级", data.wind, data.windPower));
    }
}