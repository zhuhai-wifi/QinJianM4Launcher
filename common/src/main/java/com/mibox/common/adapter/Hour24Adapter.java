package com.mibox.common.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mibox.common.R;
import com.mibox.common.bean.Hour24Brief;

public class Hour24Adapter extends BaseQuickAdapter<Hour24Brief.Hour24Data, BaseViewHolder> {

    public Hour24Adapter() {
        super(R.layout.layout_hour24_item);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder vh, Hour24Brief.Hour24Data data) {
        vh.setText(R.id.txt_temp, "" + data.temperature + "℃");
        vh.setText(R.id.txt_hour, data.time);
        ImageView iconView = vh.getView(R.id.weather_img);
        Glide.with(iconView.getContext()).load(data.icon).into(iconView);
    }
}