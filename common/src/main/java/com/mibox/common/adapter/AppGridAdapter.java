package com.mibox.common.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mibox.common.R;
import com.mibox.common.app.AppEntry;

public class AppGridAdapter extends BaseQuickAdapter<AppEntry, BaseViewHolder> {

    public AppGridAdapter() {
        super(R.layout.layout_app_item);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder vh, AppEntry app) {
        vh.setText(R.id.app_name, app.label);
        vh.setImageDrawable(R.id.app_img, app.icon);
    }
}