package com.mibox.common.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mibox.common.R;
import com.mibox.common.app.SettingsEntry;

public class SettingsGridAdapter extends BaseQuickAdapter<SettingsEntry, BaseViewHolder> {

    public SettingsGridAdapter() {
        super(R.layout.layout_settings_item);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder vh, SettingsEntry app) {
        vh.setText(R.id.settings_txt, app.label);
    }
}