package com.mibox.common.adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ColorUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mibox.common.R;
import com.mibox.common.bean.SinaStock;

public class StockGridAdapter extends BaseQuickAdapter<SinaStock, BaseViewHolder> {

    public StockGridAdapter() {
        super(R.layout.layout_base_stock);
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void convert(@NonNull BaseViewHolder vh, SinaStock data) {
        vh.setText(R.id.stock_name, data.name);
        vh.setText(R.id.stock_price, to2String(data.price));
        float wave = data.price - data.lastPrice;
        float wavePercent = wave * 100f / data.lastPrice;
        vh.setText(R.id.stock_wave, String.format("%s%%", toTitle2String(wavePercent)));
        int colorRed = ColorUtils.getColor(R.color.color_stock_red);
        int colorGreen = ColorUtils.getColor(R.color.color_stock_green);
        if (wave >= 0.0f) {
            vh.setTextColor(R.id.stock_price, colorRed);
            vh.setTextColor(R.id.stock_wave, colorRed);
        } else {
            vh.setTextColor(R.id.stock_price, colorGreen);
            vh.setTextColor(R.id.stock_wave, colorGreen);
        }
    }

    @SuppressLint("DefaultLocale")
    private static String to2String(float src) {
        return String.format("%.2f", Math.round(src * 100f) / 100.f);
    }

    @SuppressLint("DefaultLocale")
    private static String toTitle2String(float src) {
        return String.format("%+.2f", Math.round(src * 100f) / 100.f);
    }
}
