package com.mibox.common.utils.ticker;

public interface TickCallback {
    void onSecondTick(long sec);
}
