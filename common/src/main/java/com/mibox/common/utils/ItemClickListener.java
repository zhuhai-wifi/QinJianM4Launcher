package com.mibox.common.utils;

public interface ItemClickListener {
    void onClick();
}
