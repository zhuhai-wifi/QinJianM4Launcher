package com.mibox.common.utils;

import android.annotation.SuppressLint;

import com.amap.api.location.AMapLocation;
import com.mibox.common.bean.BriefDataMsg;
import com.mibox.common.bean.CityId;
import com.mibox.common.bean.Day15Brief;
import com.mibox.common.bean.Hour24Brief;
import com.mibox.common.bean.SinaStocks;
import com.mibox.common.bean.TwoDayBrief;
import com.mibox.common.consts.ApiUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class HttpApi {

    public static void getSinaStock(List<String> codes, final HttpCallback<SinaStocks> cb) {
        StringBuilder sb = new StringBuilder(ApiUrl.SINA_STOCK_BASE);
        for (String code : codes) {
            sb.append(code).append(',');
        }
        HttpHelper.INSTANCE.sinaGet(sb.toString(), cb);
    }

    @SuppressLint("DefaultLocale")
    public static void getRealTimeBrief(int cityId, HttpCallback<BriefDataMsg> cb) {
        String url = String.format(ApiUrl.GET_REALTIME_BRIEF, cityId);
        HttpHelper.INSTANCE.get(url, cb);
    }

    @SuppressLint("DefaultLocale")
    public static void get24HourBrief(int cityId, HttpCallback<Hour24Brief> cb) {
        String url = String.format(ApiUrl.GET_WEATHER_24, cityId);
        HttpHelper.INSTANCE.get(url, cb);
    }

    @SuppressLint("DefaultLocale")
    public static void get15DaysBrief(int cityId, HttpCallback<Day15Brief> cb) {
        String url = String.format(ApiUrl.GET_BRIEF_DAY_15, cityId);
        HttpHelper.INSTANCE.get(url, cb);
    }

    @SuppressLint("DefaultLocale")
    public static void getTwoDayBrief(int cityId, HttpCallback<TwoDayBrief> cb) {
        String url = String.format(ApiUrl.GET_DAILY_BRIEF, cityId);
        HttpHelper.INSTANCE.get(url, cb);
    }

    public static void getCityId(AMapLocation loc, HttpCallback<CityId> cb) {
        JSONObject jsObj = new JSONObject();
        try {
            jsObj.put("city", loc.getCity());
            jsObj.put("district", loc.getDistrict());
            jsObj.put("lat", loc.getLatitude());
            jsObj.put("lon", loc.getLongitude());
            jsObj.put("nation", loc.getCountry());
            jsObj.put("province", loc.getProvince());
            HttpHelper.INSTANCE.postJson(ApiUrl.GET_CITY_ID, jsObj.toString(), cb);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
