package com.mibox.common.utils.location;

import com.amap.api.location.AMapLocation;

public interface AMapCallback {
    void onLocationGet(AMapLocation location);
}
