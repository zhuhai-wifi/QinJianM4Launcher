package com.mibox.common.utils;

import java.lang.reflect.ParameterizedType;

public abstract class HttpCallback<T> {

    public Class<T> entityClass = (Class<T>) ((ParameterizedType)
            getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    public abstract void onSuccess(T res);

    public abstract void onFail(String exp);

}
