package com.mibox.common.consts;

public class ApiUrl {
    //https://zhwnl.weatherat.com/
    public static final String GET_CITY_ID = "https://data.weatherat.com/location/getCityId";
    public static final String GET_REALTIME_BRIEF = "https://api.weatherat.com/frontend/realtime/brief/%d";
    public static final String GET_DAILY_BRIEF = "https://api.weatherat.com/frontend/daily/brief/%d";

    public static final String GET_WEATHER_24 = "https://api.weatherat.com/frontend/hourly/24hour/%d";
    public static final String GET_BRIEF_DAY_15 = "https://api.weatherat.com/frontend/daily/15day/%d";

    //sina stock
    public static final String SINA_APP_USER_AGENT = "sinafinance__5.17.0.2__android__e1a35dbaa58ec29e__11__OPPO+PEAM10";
    public static final String SINA_STOCK_BASE = "https://b.sinajs.cn/?format=text&list=";
}
