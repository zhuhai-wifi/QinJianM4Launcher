package com.mibox.common.app;

import com.mibox.common.utils.ItemClickListener;

public class SettingsEntry {
    public String label;
    public ItemClickListener listener;

    public SettingsEntry(String label, ItemClickListener listener) {
        this.label = label;
        this.listener = listener;
    }
}
