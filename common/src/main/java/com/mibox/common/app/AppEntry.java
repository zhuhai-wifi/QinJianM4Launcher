
package com.mibox.common.app;

import android.graphics.drawable.Drawable;

public class AppEntry {
    public String dataDir;
    public Drawable icon;
    public String label;
    public String launcherName;
    public String packageName;
    public boolean isSysApp;
    public boolean isFake;
}
