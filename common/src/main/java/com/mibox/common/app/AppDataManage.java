
package com.mibox.common.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppDataManage {

    private final Context mContext;

    public AppDataManage(Context context) {
        mContext = context;
    }

    public ArrayList<AppEntry> getLaunchAppList() {
        PackageManager localPackageManager = mContext.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> localList = localPackageManager.queryIntentActivities(localIntent, 0);
        ArrayList<AppEntry> apps;
        Iterator<ResolveInfo> localIterator;
        apps = new ArrayList<>();
        if (localList.size() != 0) {
            localIterator = localList.iterator();
            while (localIterator.hasNext()) {
                ResolveInfo localResolveInfo = localIterator.next();
                AppEntry appEntry = new AppEntry();
                appEntry.icon = localResolveInfo.activityInfo.loadIcon(localPackageManager);
                appEntry.label = localResolveInfo.activityInfo.loadLabel(localPackageManager).toString();
                appEntry.packageName = localResolveInfo.activityInfo.packageName;
                appEntry.dataDir = localResolveInfo.activityInfo.applicationInfo.publicSourceDir;
                appEntry.launcherName = localResolveInfo.activityInfo.name;
                appEntry.isFake = false;
                PackageInfo mPackageInfo;
                try {
                    mPackageInfo = mContext.getPackageManager().getPackageInfo(appEntry.packageName, 0);
                    if ((mPackageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) > 0) {// 系统预装
                        appEntry.isSysApp = true;
                    }
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
                apps.add(appEntry);
            }
        }
        return apps;
    }
}
