package com.mibox.common.event;

public class PackageEvent {
    public String action;
    public String packageName;

    public PackageEvent(String action, String packageName) {
        this.action = action;
        this.packageName = packageName;
    }
}
