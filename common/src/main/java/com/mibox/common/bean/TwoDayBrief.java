package com.mibox.common.bean;

import java.util.List;

public class TwoDayBrief extends BaseMsg {

    public List<EachDayData> data;

    public static class EachDayData {
        public String icon;
        public String week;
        public String date;
        public int minTemperature;
        public int maxTemperature;
    }
}
