package com.mibox.common.bean;

import java.util.List;

public class Day15Brief {

    public List<Day15Data> data;
    public int status;

    public static class Day15Data {
        public int aqiLevel;
        public String date;
        public String dayCondition;
        public String dayIcon;
        public int maxTemperature;
        public int minTemperature;
        public String nightCondition;
        public String nightIcon;
        public String week;
        public String wind;
        public int windPower;
    }
}
