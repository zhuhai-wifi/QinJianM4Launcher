package com.mibox.common.bean;

import java.util.List;

public class Hour24Brief {

    public List<Hour24Data> data;
    public int status;

    public static class Hour24Data {
        public String icon;
        public String time;
        public int windPower;
        public int temperature;
    }
}
