package com.mibox.common.bean;

import com.blankj.utilcode.util.StringUtils;

public class SinaStock {
    //sh688036=传音控股,135.460,135.430,133.120,138.100,132.200,133.120,133.150,1672572,223929395.000,
    // 2866,133.120,5000,133.110,5397,133.100,1397,133.090,831,133.080,82,133.150,200,133.360,809,133.380,
    // 700,133.400,300,133.430,2022-02-07,15:29:59,00,D|0|0.00
    public String code;
    public String name;
    public Float price;
    public Float startPrice;
    public Float lastPrice;

    public static SinaStock fromString(String str) {
        if (StringUtils.isTrimEmpty(str)) {
            return null;
        }
        SinaStock stock = new SinaStock();
        try {
            String[] subs = str.split("=");
            stock.code = subs[0];
            subs = subs[1].split(",");
            stock.name = subs[0];
            stock.startPrice = Float.parseFloat(subs[1]);
            stock.lastPrice = Float.parseFloat(subs[2]);
            stock.price = Float.parseFloat(subs[3]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stock;
    }
}
