package com.mibox.common.bean;

import com.blankj.utilcode.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class SinaStocks {
    public List<SinaStock> data;

    public static SinaStocks fromString(String str) {
        SinaStocks stocks = new SinaStocks();
        if (!StringUtils.isTrimEmpty(str)) {
            String[] lines = str.split("\n");
            List<SinaStock> data = new LinkedList<>();
            for (String line : lines) {
                if (!StringUtils.isTrimEmpty(line)) {
                    data.add(SinaStock.fromString(line.trim()));
                }
            }
            stocks.data = data;
        }
        return stocks;
    }
}
