package com.mibox.common.bean;

public class BriefDataMsg extends BaseMsg{
    public BriefData data;

    public static class BriefData {
        public String condition;
        public String airCondition;
        public String wind;
        public int windPower;
        public int temperature;
        public int aqi;
        public int humidity;
    }
}
