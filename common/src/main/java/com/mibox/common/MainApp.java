package com.mibox.common;

import android.app.Application;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.mibox.common.utils.ticker.AppTicker;
import com.tencent.mmkv.MMKV;
import com.tencent.mmkv.MMKVLogLevel;

public class MainApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppTicker.INSTANCE.start();
        Utils.init(this);
        MMKV.initialize(this);
        MMKV.setLogLevel(MMKVLogLevel.LevelNone);
        LogUtils.getConfig().setBorderSwitch(false);
    }

    @Override
    public void onTerminate() {
        AppTicker.INSTANCE.stop();
        super.onTerminate();
    }
}
